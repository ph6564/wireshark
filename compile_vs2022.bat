set PLATFORM=win64
set WIRESHARK_LIB_DIR=D:\wireshark\wireshark-win64-libs
set CMAKE_PREFIX_PATH=d:\Qt\6.4.0\msvc2019_64
set QT5_BASE_DIR=d:\Qt\6.4.0\msvc2019_64
set QT6Core_DIR = D:\Qt\6.4.0\msvc2019_64\lib\cmake\Qt6Core
set QT6CoreTools_DIR = D:\Qt\6.4.0\msvc2019_64\lib\cmake\Qt6CoreTools
set  WIRESHARK_QT6_PREFIX_PATH=d:\Qt\6.4.0\msvc2019_64
#set USE_qt6=ON
cmake -G "Visual Studio 17 2022" -A x64 D:\wireshark
