/* cmakeconfig.h.in */

#ifndef __CONFIG_H__
#define __CONFIG_H__

/* Note: You cannot use earlier #defines in later #cmakedefines (cmake 2.6.2). */

/* Name of package */
#define PACKAGE "wireshark"

#define VERSION_EXTRA ""

/* Version number of Wireshark and associated utilities */
#define VERSION "3.7.3"
#define VERSION_MAJOR 3
#define VERSION_MINOR 7
#define VERSION_MICRO 3

/* Version number of Logray and associated utilities */
#define LOG_VERSION "0.8.3"

#define PLUGIN_PATH_ID "3.7"
#define VERSION_FLAVOR "Development Build"

/* Build wsutil with SIMD optimization */
#define HAVE_SSE4_2 1

/* Define to 1 if we want to enable plugins */
#define HAVE_PLUGINS 1

/*  Define to 1 if we check hf conflict */
/* #undef ENABLE_CHECK_FILTER */

/* Link Wireshark libraries statically */
/* #undef ENABLE_STATIC */

/* Enable AirPcap */
#define HAVE_AIRPCAP 1

/* Define to 1 if you have the <arpa/inet.h> header file. */
/* #undef HAVE_ARPA_INET_H */

/* Define to 1 if you have the `clock_gettime` function. */
/* #undef HAVE_CLOCK_GETTIME */

/* Define to 1 if you have the `timespec_get` function. */
#define HAVE_TIMESPEC_GET 1

/* Define to use the MaxMind DB library */
#define HAVE_MAXMINDDB 1

/* Define to 1 if you have the <ifaddrs.h> header file. */
/* #undef HAVE_IFADDRS_H */

/* Define to 1 if yu have the `fseeko` function. */
/* #undef HAVE_FSEEKO */

/* Define to 1 if you have the `getexecname' function. */
/* #undef HAVE_GETEXECNAME */

/* Define to 1 if you have the `getifaddrs' function. */
/* #undef HAVE_GETIFADDRS */

/* Define if LIBSSH support is enabled */
#define HAVE_LIBSSH 1

/* Define if you have the 'dlget' function. */
/* #undef HAVE_DLGET */

/* Define to 1 if you have the <grp.h> header file. */
/* #undef HAVE_GRP_H */

/* Define to use heimdal kerberos */
/* #undef HAVE_HEIMDAL_KERBEROS */

/* Define to 1 if you have the `krb5_pac_verify' function. */
#define HAVE_KRB5_PAC_VERIFY 1

/* Define to 1 if you have the `krb5_c_fx_cf2_simple' function. */
#define HAVE_KRB5_C_FX_CF2_SIMPLE 1

/* Define to 1 if you have the `decode_krb5_enc_tkt_part' function. */
/* #undef HAVE_DECODE_KRB5_ENC_TKT_PART */

/* Define to 1 if you have the `encode_krb5_enc_tkt_part' function. */
/* #undef HAVE_ENCODE_KRB5_ENC_TKT_PART */

/* Define to 1 if you have the `inflatePrime' function. */
#define HAVE_INFLATEPRIME 1

/* Define to 1 if you have the `issetugid' function. */
/* #undef HAVE_ISSETUGID */

/* Define to use kerberos */
#define HAVE_KERBEROS 1

/* Define to use PCRE2 library */
#define HAVE_PCRE2 1

/* Define to use nghttp2 */
#define HAVE_NGHTTP2 1

/* Define to use the libcap library */
/* #undef HAVE_LIBCAP */

/* Define to use GnuTLS library */
#define HAVE_LIBGNUTLS 1

/* Define to 1 if GnuTLS was built with pkcs11 support. */
#define HAVE_GNUTLS_PKCS11 1

/* Enable libnl support */
/* #undef HAVE_LIBNL */

/* libnl version 1 */
/* #undef HAVE_LIBNL1 */

/* libnl version 2 */
/* #undef HAVE_LIBNL2 */

/* libnl version 3 */
/* #undef HAVE_LIBNL3 */

/* Define to use libpcap library */
#define HAVE_LIBPCAP 1

/* Define to 1 if you have the `smi' library (-lsmi). */
#define HAVE_LIBSMI 1

/* Define to 1 if libsmi exports a version string (and that symbol is visible). */
/* #undef HAVE_SMI_VERSION_STRING */

/* Define to use zlib library */
#define HAVE_ZLIB 1

/* Define to use the minizip library */
#define HAVE_MINIZIP 1

/* Define if `dos_date' (with underscore) field exists in `zip_fileinfo'  */
/* #undef HAVE_MZCOMPAT_DOS_DATE */

/* Define to use brotli library */
#define HAVE_BROTLI 1

/* Define to use lz4 library */
#define HAVE_LZ4 1

/* Check for lz4frame */
#define HAVE_LZ4FRAME_H 1

/* Define to use snappy library */
#define HAVE_SNAPPY 1

/* Define to use zstd library */
#define HAVE_ZSTD 1

/* Define to 1 if you have the <linux/sockios.h> header file. */
/* #undef HAVE_LINUX_SOCKIOS_H */

/* Define to 1 if you have the <linux/if_bonding.h> header file. */
/* #undef HAVE_LINUX_IF_BONDING_H */

/* Define to use Lua */
#define HAVE_LUA 1

/* Define to use MIT kerberos */
#define HAVE_MIT_KERBEROS 1

/* Define to 1 if you have the <netdb.h> header file. */
/* #undef HAVE_NETDB_H */

/* Define to 1 if you have the <netinet/in.h> header file. */
/* #undef HAVE_NETINET_IN_H */

/* nl80211.h is new enough */
/* #undef HAVE_NL80211 */

/* SET_CHANNEL is supported */
/* #undef HAVE_NL80211_CMD_SET_CHANNEL */

/* SPLIT_WIPHY_DUMP is supported */
/* #undef HAVE_NL80211_SPLIT_WIPHY_DUMP */

/* VHT_CAPABILITY is supported */
/* #undef HAVE_NL80211_VHT_CAPABILITY */

/* Define to 1 if you have macOS frameworks */
/* #undef HAVE_MACOS_FRAMEWORKS */

/* Define to 1 if you have the macOS CFPropertyListCreateWithStream function */
/* #undef HAVE_CFPROPERTYLISTCREATEWITHSTREAM */

/* Define to 1 if you have the `pcap_create' function. */
#define HAVE_PCAP_CREATE 1

/* Define to 1 if the capture buffer size can be set. */
#define CAN_SET_CAPTURE_BUFFER_SIZE 1

/* Define to 1 if you have the `pcap_freecode' function. */
#define HAVE_PCAP_FREECODE 1

/* Define to 1 if you have the `pcap_free_datalinks' function. */
#define HAVE_PCAP_FREE_DATALINKS 1

/* Define to 1 if you have the `pcap_open' function. */
#define HAVE_PCAP_OPEN 1

/* Define to 1 if you have libpcap/WinPcap/Npcap remote capturing support. */
#define HAVE_PCAP_REMOTE 1

/* Define to 1 if you have the `pcap_setsampling' function. */
#define HAVE_PCAP_SETSAMPLING 1

/* Define to 1 if you have the `pcap_set_tstamp_precision' function. */
#define HAVE_PCAP_SET_TSTAMP_PRECISION 1

/* Define to 1 if you have the `pcap_set_tstamp_type' function. */
#define HAVE_PCAP_SET_TSTAMP_TYPE 1

/* Define to 1 if you have the <pwd.h> header file. */
/* #undef HAVE_PWD_H */

/* Define to 1 if you want to playing SBC by standalone BlueZ SBC library */
#define HAVE_SBC 1

/* Define to 1 if you have the SpanDSP library. */
#define HAVE_SPANDSP 1

/* Define to 1 if you have the bcg729 library. */
#define HAVE_BCG729 1

/* Define to 1 if you have the ilbc library. */
#define HAVE_ILBC 1

/* Define to 1 if you have the opus library. */
#define HAVE_OPUS 1

/* Define to 1 if you have the speexdsp library. */
/* #undef HAVE_SPEEXDSP */

/* Define to 1 if you have the lixbml2 library. */
#define HAVE_LIBXML2 1

/* Define to 1 if you have the `setresgid' function. */
/* #undef HAVE_SETRESGID */

/* Define to 1 if you have the `setresuid' function. */
/* #undef HAVE_SETRESUID */

/* Define to 1 if you have the Sparkle or WinSparkle library */
#define HAVE_SOFTWARE_UPDATE 1

/* Define if you have the 'strptime' function. */
/* #undef HAVE_STRPTIME */

/* Define if you have the 'memmem' function. */
/* #undef HAVE_MEMMEM */

/* Define if you have the 'strcasestr' function. */
/* #undef HAVE_STRCASESTR */

/* Define if you have the 'strerrorname_np' function. */
/* #undef HAVE_STRERRORNAME_NP */

/* Define if you have the 'vasprintf' function. */
/* #undef HAVE_VASPRINTF */

/* Define to 1 if `st_birthtime' is a member of `struct stat'. */
/* #undef HAVE_STRUCT_STAT_ST_BIRTHTIME */

/* Define if st_blksize field exists in struct stat */
/* #undef HAVE_STRUCT_STAT_ST_BLKSIZE */

/* Define to 1 if `__st_birthtime' is a member of `struct stat'. */
/* #undef HAVE_STRUCT_STAT___ST_BIRTHTIME */

/* Define to 1 if you have the <sys/socket.h> header file. */
/* #undef HAVE_SYS_SOCKET_H */

/* Define to 1 if you have the <sys/time.h> header file. */
/* #undef HAVE_SYS_TIME_H */

/* Define to 1 if you have the <sys/utsname.h> header file. */
/* #undef HAVE_SYS_UTSNAME_H */

/* Define to 1 if you have the <sys/wait.h> header file. */
/* #undef HAVE_SYS_WAIT_H */

/* Define if tm_zone field exists in struct tm */
/* #undef HAVE_STRUCT_TM_TM_ZONE */

/* Define if tzname array exists */
/* #undef HAVE_TZNAME */

/* Define to 1 if you have the <unistd.h> header file. */
/* #undef HAVE_UNISTD_H */

/* Define if we have QtMultimedia */
#define QT_MULTIMEDIA_LIB 1

/* Build androiddump with libpcap instead of wireshark stuff */
/* #undef ANDROIDDUMP_USE_LIBPCAP */

/* Define to 1 if `lex' declares `yytext' as a `char *' by default, not a
   `char[]'. */
/* Note: not use in the code */
/* #undef YYTEXT_POINTER */

/* Define to 1 if the 'ssize_t' type exists. */
/* #undef HAVE_SSIZE_T */

#if defined(_MSC_VER)
#  define strncasecmp strnicmp
#  define popen       _popen
#  define pclose      _pclose
#endif

#if defined(_WIN32)
   /* WpdPack/INclude/pcap/pcap.h checks for "#if defined(WIN32)" */
#  ifndef WIN32
#    define WIN32	1
#  endif

   /*
    * Flex (v 2.5.35) uses this symbol to "exclude" unistd.h
    */
#  define YY_NO_UNISTD_H

#  ifndef __STDC__
#    define __STDC__ 0
#  endif
#endif

#ifdef HAVE_PCRE2
#define PCRE2_CODE_UNIT_WIDTH  8
#endif

#include <ws_log_defs.h>

#endif /* __CONFIG_H__ */
