# ============================================================================
# NSIS configuration definitions. Generated from wireshark-config.nsh.in.
# ============================================================================

# Do not prefix comments with ";". They will be removed by CMake.

# MUST match "<Product ... Name=" in wix/Wireshark.wxs.
!define PROGRAM_NAME "Wireshark"
!define TOP_SRC_DIR "D:\wireshark"
!define WIRESHARK_TARGET_PLATFORM win64
!define TARGET_MACHINE x64
!define EXTRA_INSTALLER_DIR "D:\wireshark\wireshark-win64-libs"
!define NPCAP_PACKAGE_VERSION 1.60
!define USBPCAP_PACKAGE_VERSION 1.5.4.0
!define VERSION 3.7.3
!define MAJOR_VERSION 3
!define MINOR_VERSION 7
!define PRODUCT_VERSION 3.7.3.0

!define VCREDIST_DIR "D:/vs2022/VC/Redist/MSVC/14.32.31326"
!define VCREDIST_EXE "vc_redist.x64.exe"

# Optional components

!define MMDBRESOLVE_EXE TRUE

!define DOCBOOK_DIR "D:\wireshark\docbook"

!define SMI_DIR "D:/wireshark/wireshark-win64-libs/libsmi-svn-40773-win64ws"

!define QT_DIR "${STAGING_DIR}"
